cd ./web-client-react
npm install
npm run build
cd ..
rm -ri public/static
cp -R ./web-client-react/build/static public/

cp .env.example .env
echo "APP_KEY=$APP_KEY" >> .env
echo "DB_DATABASE=$DB_DATABASE" >> .env
echo "DB_PASSWORD=$DB_PASSWORD" >> .env
echo "DB_USERNAME=$DB_USERNAME" >> .env

