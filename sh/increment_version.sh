CurrentVersion=`git tag | tail -1`
Prefix=`echo $CurrentVersion | grep -o "\d\.\d."`
MinorVersion=`echo $CurrentVersion | grep -o "\d*$"`
NewMinorVersion=$((MinorVersion+1))
NewVersion="v$Prefix$NewMinorVersion"
git config --global user.name "Pipeline"
git config --global user.email "build@pipeline.com"
git tag -a "$NewVersion" -m "Added version $NewVersion"
