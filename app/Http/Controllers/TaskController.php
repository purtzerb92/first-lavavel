<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Task as TaskModel;

class TaskController extends Controller
{
    public function showTask($id) {
        error_log("WITHIN SHOW TASK $id");
        $task = TaskModel::find($id);
        error_log(json_encode($task));
        return view('task', ['task' => 'test']);
    }

}
