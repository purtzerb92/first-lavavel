FROM php:8.1.0-cli
COPY . /usr/src/myapp
WORKDIR /usr/src/myapp
RUN apt update
RUN apt install zip unzip
RUN docker-php-ext-install mysqli pdo pdo_mysql
RUN ./sh/composer i
EXPOSE 8000
CMD php artisan serve --host 0.0.0.0
