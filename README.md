## About the application
This is a sample app that I have been using to learn. It is a mix of different technologies, and I will update here as needed as I add more. The core framework is utilizing PHP and laravel.

## To Run In Development
- Pull down application
- run <code>php artisan install</code>
- run <code>php artisan migrate</code> to create the necessary tables
- run <code>docker compose up db</code>
- [Configure DB](#configure-db)
- run <code>cd web-client-react</code>
- run <code>npm install</code>
- run <code>npm run build</code>
- run <code>cd ..</code>
- run <code>php artisan serve</code>
- Go to localhost:8000


## Configure docker DB
You will need a db connection for the app to work well. For now, the only db work I have is some tasks and some player stuff for the react app I am building. The docker compose file includes configuration for a mysql image. You can run this one, but to connect from your local host, you have to change some user permissions within mysql, or create a new user besides root to connect to mysql. <br /><br />

#### Setup database
- If the docker image is not running, run <code>docker compose up -d db</code>
- Get into container <code>docker exec -it db bash</code>
- Connect to the mysql <code>mysql -u root -h 127.0.0.1 -p</code>
   - Enter password within docker-compose.yml (testing1234)
- Create database <code>CREATE DATABASE app;</code>

#### To use root:
- If the docker image is not running, run <code>docker compose up -d db</code>
- Get into container <code>docker exec -it db bash</code>
- Connect to the mysql <code>mysql -u root -h 127.0.0.1 -p</code>
   - Enter password within docker-compose.yml (testing1234)
- Update host rule <code>update mysql.user set host = ' '  where user = 'root'; flush privileges;</code>
- Now you should be able to connect to the db image on port 3307

## To Run In Prod
- Maybe will come if I decide to deploy :) But shouldn't be hard with how docker is set up.

## React
I enjoy react, and was curious how I would integrate it within a laravel application. My plan was to have some routes lead to a react application, while still maintaining the current php routes. 

<h4>How it works</h4>
The react application is conrtained within <code>web-client-react</code>. You can develop from this independently, but you would have to run the laravel application as well to support the backend. So some port mapping would be necessary, but that could be isolated to the <code>web-client-react</code>. <br /><br />
I still need to hoist commands to artisan, but currently, you would need to build the application within <code>web-client-react</code> with npm run build. This will build the application, and then copy over the <code>web-client-react/build/static</code> folder into <code>/public</code> for access from laravel. It will also copy <code>web-client-react/build/index.html</code> to <code>/resources/views/reactApp.blade.php</code><br /><br />
Once those files are in place, <code>routes/web.php</code> renders this view for the desired routes, in this case <code>reactApp/*</code>. React's internal routing can handle rendering different pages, and when it directs to a laravel page, it will be handled by other laravel routes.<br /><br />

## About Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel takes the pain out of development by easing common tasks used in many web projects, such as:

- [Simple, fast routing engine](https://laravel.com/docs/routing).
- [Powerful dependency injection container](https://laravel.com/docs/container).
- Multiple back-ends for [session](https://laravel.com/docs/session) and [cache](https://laravel.com/docs/cache) storage.
- Expressive, intuitive [database ORM](https://laravel.com/docs/eloquent).
- Database agnostic [schema migrations](https://laravel.com/docs/migrations).
- [Robust background job processing](https://laravel.com/docs/queues).
- [Real-time event broadcasting](https://laravel.com/docs/broadcasting).

Laravel is accessible, powerful, and provides tools required for large, robust applications.

## Learning Laravel

Laravel has the most extensive and thorough [documentation](https://laravel.com/docs) and video tutorial library of all modern web application frameworks, making it a breeze to get started with the framework.

If you don't feel like reading, [Laracasts](https://laracasts.com) can help. Laracasts contains over 1500 video tutorials on a range of topics including Laravel, modern PHP, unit testing, and JavaScript. Boost your skills by digging into our comprehensive video library.

